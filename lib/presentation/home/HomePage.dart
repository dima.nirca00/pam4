import 'package:app_1/presentation/home/controller/GoalsController.dart';
import 'package:app_1/presentation/home/widgets/daily_exercise.dart';
import 'package:app_1/presentation/home/widgets/goals_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controller/ExerciseController.dart';
class HomePage extends StatefulWidget {
  HomePage({super.key});
  @override
  _HomePage createState() => _HomePage();
}
class _HomePage extends State<HomePage> {
  _HomePage();
  final goalsController = Get.put(GoalsController());
  final exerciseController = Get.put(ExerciseController());
  @override
  void initState() {
    Get.lazyPut(() => GoalsController());
    Get.lazyPut(() => ExerciseController());
    GoalsController goalsController = Get.find();
    ExerciseController exerciseController = Get.find();
    goalsController.readJsonGoals();
    exerciseController.readJsonGoals();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    GoalsController goalsController = Get.find();
    ExerciseController exerciseController = Get.find();
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0.0), // here the desired height
            child: AppBar()),
        body: Column(
          children: <Widget>[
            Obx( () => goalsController.goalsList.isNotEmpty
                ? GoalsSection(goals: goalsController.goalsList)
                : const Center(child: CircularProgressIndicator())
            ),
            Obx( () => exerciseController.exerciseList.isNotEmpty
                ? DailyTaskSection(exercises: exerciseController.exerciseList)
                : const Center(child: CircularProgressIndicator())
            )
          ],
        )
    );
  }
}