import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class GoalsDataModel{
  final String cover;
  final String title;
  @JsonKey(name:'sub_title')
  final String subTitle;
  @JsonKey(name:'calories_count')
  final int caloriesCount;
  @JsonKey(name:'duration_seconds')
  final int durationSeconds;

  GoalsDataModel({
    required this.cover,
    required this.title,
    required this.subTitle,
    required this.caloriesCount,
    required this.durationSeconds
  });

}
