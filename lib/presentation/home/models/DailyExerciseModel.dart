import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()

class DailyExerciseModel{
  final String title;
  final String cover;
  @JsonKey(name:'calories_count')
  final int caloriesCount;
  @JsonKey(name:'duration_seconds')
  final int durationSeconds;


  DailyExerciseModel({
    required this.title,
    required this.cover,
    required this.caloriesCount,
    required this.durationSeconds});

}