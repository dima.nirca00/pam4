import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../models/GoalsDataModel.dart';

class GoalsController extends GetxController{
  RxList<GoalsDataModel> goalsList = RxList();
  Future<void> readJsonGoals() async{
    final  String response = await rootBundle.loadString('resources/files/fitness.json');
    final data = await json.decode(response);
    data["goals"].forEach((item){
      goalsList.add(GoalsDataModel(
        cover: item["cover"],
        title: item["title"],
        subTitle: item["sub_title"],
        caloriesCount: item["calories_count"],
        durationSeconds: item["duration_seconds"]
      ));
    });
  }
}
