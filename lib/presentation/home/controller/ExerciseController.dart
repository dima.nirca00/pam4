import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../models/DailyExerciseModel.dart';

class ExerciseController extends GetxController{
  RxList<DailyExerciseModel> exerciseList = RxList();
  Future<void> readJsonGoals() async{
    final  String response = await rootBundle.loadString('resources/files/fitness.json');
    final data = await json.decode(response);
    data["daily_exercises"].forEach((item){
      exerciseList.add(DailyExerciseModel(
          title: item["title"],
          cover: item["cover"],
          caloriesCount: item["calories_count"],
          durationSeconds: item["duration_seconds"]
      ));
    });
  }
}
