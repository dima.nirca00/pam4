import 'package:app_1/presentation/home/models/GoalsDataModel.dart';
import 'package:app_1/presentation/home/widgets/title.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

import '../../../resources/custom_colors.dart';
import '../../../resources/constants.dart';
import '../../../resources/text_styles.dart';


class GoalsSection extends StatefulWidget {
  RxList<GoalsDataModel> goals;
  GoalsSection({super.key, required this.goals});
  @override
  _GoalsSection createState() => _GoalsSection(this.goals);
}
class _GoalsSection extends State<GoalsSection> {
  RxList<GoalsDataModel> goals;

  _GoalsSection(this.goals);

  // int formatTime(int seconds) {
  //   return Duration(seconds: seconds).inMinutes;
  // }
  @override
  Widget build(BuildContext context){
  return Column (
      children: <Widget> [
        TitleWidget.title(Strings.goalSectionTitle),
        _goalBody()
  ]);
  }

  SizedBox _goalBody() {
    return SizedBox(
      height:276,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: goals.isEmpty ? 0:goals.length,
        itemBuilder: (context, index){
          return Card(
              elevation:0,
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
              child: Container(
                //margin: EdgeInsets.(5),
                  height:276,
                  width:310,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(24)),
                      color: Colors.white,
                      boxShadow:[
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 15.0, // soften the shadow
                          spreadRadius: 0.0, //extend the shadow
                          offset: Offset(
                            5.0, // Move to right 5  horizontally
                            5.0, // Move to bottom 5 Vertically
                          ),
                        )
                      ]
                  ),

                  child: Stack(
                    children: <Widget>[
                      Positioned(
                          child:ClipRRect(
                            borderRadius: BorderRadius.circular(24),
                            child: Image(
                                width:310,
                                image:NetworkImage(goals[index].cover.toString()),height: 144,fit:BoxFit.cover),
                          )
                      ),
                      Positioned(
                        left:12,
                        top:160,
                        child:Text(goals[index].title.toString(),style:TextStyles.textStylePoppins24(color:Colors.black)),
                      ),
                      Positioned(
                          left:12,
                          top:196,
                          child:Text(goals[index].subTitle.toString(),style:TextStyles.textStylePoppins14M(color:CustomColors.grey),
                          )),
                      Positioned(
                        left:226,
                        top:104,
                        child: SvgPicture.asset('resources/images/play.svg'),
                      ),
                      Positioned(
                        left:12,
                        bottom:16,
                        child: Container(
                          height:32,
                          width:85,
                          decoration: BoxDecoration(
                              color: CustomColors.sugarWhite, // border color
                              shape: BoxShape.rectangle,
                              border: Border.all(
                                color: CustomColors.saladGreen,
                                width: 1,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(16))
                          ),
                        ),
                      ),
                      Positioned(
                        left:22,
                        top:225,
                        child: SvgPicture.asset('resources/images/time.svg'),
                      ),
                      Positioned(
                          left:40,
                          top:224,
                          child:Text((goals[index].durationSeconds!/3600).toStringAsFixed(1),style:TextStyles.textStylePoppins14M(color:CustomColors.saladGreen),
                          )),
                      Positioned(
                          left:62,
                          top:224,
                          child:Text('hour',style:TextStyles.textStylePoppins14M(color:CustomColors.saladGreen),
                          )),
                      Positioned(
                        left:113,
                        bottom:16,
                        child: Container(
                          height:32,
                          width:85,
                          decoration: BoxDecoration(
                              color: CustomColors.snowWhite,
                              shape: BoxShape.rectangle,
                              border: Border.all(
                                color: CustomColors.eggYellow,
                                width: 1,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(16))
                          ),
                        ),
                      ),
                      Positioned(
                        left:124,
                        top:225,
                        child: SvgPicture.asset('resources/images/calories.svg'),
                      ),
                      Positioned(
                          left:141,
                          top:224,
                          child:Text(goals[index].caloriesCount.toString(),style:TextStyles.textStylePoppins14M(color:CustomColors.eggYellow),
                          )),
                      Positioned(
                          left:168,
                          top:224,
                          child:Text('cal',style:TextStyles.textStylePoppins14M(color:CustomColors.eggYellow),
                          )),
                    ],
                  )

              )

          );
        },
      ),
    );
  }
}