import 'package:app_1/presentation/home/widgets/title.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

import '../../../resources/constants.dart';
import '../../../resources/custom_colors.dart';
import '../../../resources/text_styles.dart';
import '../models/DailyExerciseModel.dart';

class DailyTaskSection extends StatefulWidget {
  RxList<DailyExerciseModel> exercises;
  DailyTaskSection({super.key, required this.exercises});
  @override
  _DailyTaskSection createState() => _DailyTaskSection(this.exercises);
}
class _DailyTaskSection extends State<DailyTaskSection> {
  RxList<DailyExerciseModel> exercises;

  _DailyTaskSection(this.exercises);

  // int formatTime(int seconds) {
  //   return Duration(seconds: seconds).inMinutes;
  // }
  @override
  Widget build(BuildContext context){
    return Column(
      children: <Widget>[
        TitleWidget.title(Strings.dailyTaskSectionTitle),
        _taskBody()
      ],
    );
  }

  SizedBox _taskBody() {
    return SizedBox(
        height: 280,
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: exercises.isEmpty ? 0:exercises.length,
            itemBuilder: (context, index){
              return Card(
                  elevation:0,
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                  child: Container(
                      height:48,
                      width:358,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Stack(
                          children: <Widget>[
                            Positioned(
                                child:ClipRRect(
                                  borderRadius: BorderRadius.circular(8),
                                  child: Image(
                                      width:48,
                                      image:NetworkImage(exercises[index].cover.toString()),height: 48,fit:BoxFit.cover),
                                )
                            ),
                            Positioned(
                                left:80,
                                top:0,
                                child:Text(exercises[index].title.toString(),style:TextStyles.textStylePoppins16(color:Colors.black),
                                )),
                            Positioned(
                              left:82,
                              bottom:2,
                              child: SvgPicture.asset('resources/images/time.svg'),
                            ),
                            Positioned(
                                left:100,
                                bottom:1,
                                child:Text((exercises[index].durationSeconds!/60).toStringAsFixed(1),style:TextStyles.textStylePoppins14M(color:CustomColors.saladGreen),
                                )),
                            Positioned(
                                left:125,
                                bottom:1,
                                child:Text('min',style:TextStyles.textStylePoppins14M(color:CustomColors.saladGreen),
                                )),
                            Positioned(
                              left:167,
                              bottom:2,
                              child: SvgPicture.asset('resources/images/calories.svg'),
                            ),
                            Positioned(
                                left:183,
                                bottom:1,
                                child:Text(exercises[index].caloriesCount.toString(),style:TextStyles.textStylePoppins14M(color:CustomColors.eggYellow),
                                )),
                            Positioned(
                                left:210,
                                bottom:1,
                                child:Text('cal',style:TextStyles.textStylePoppins14M(color:CustomColors.eggYellow),
                                )),
                            Positioned(
                              right:16,
                              top: 6,
                              child: SvgPicture.asset('resources/images/play2.svg'),
                            ),
                          ])
                  )
              );
            }
        )
    );
  }
}