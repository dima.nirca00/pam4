import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../resources/constants.dart';
import '../../../resources/custom_colors.dart';
import '../../../resources/text_styles.dart';

class TitleWidget {
  static Stack title(String text) {
    return Stack(
        children:[Positioned(
            child: Padding(
                padding: EdgeInsets.only(top: 0, bottom: 0, left: 16, right: 16),
                child: Container(
                    height: 50,
                    child:Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children:[
                        Text(
                          text,
                          style: TextStyles.textStylePoppins18(color: Colors.black),
                          textAlign:TextAlign.right
                          ,),
                        Text(
                            Strings.seeAllAction,
                            style: TextStyles.textStylePoppins14(color: CustomColors.opalBlue),
                            textAlign:TextAlign.right),
                      ],))))]);
  }
}