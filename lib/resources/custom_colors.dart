import 'package:flutter/cupertino.dart';

class CustomColors {
  static Color dark =  const Color(0xFF272727);
  static Color opalBlue = const Color(0xFF198FB3);
  static Color grey = const Color(0xFF949E9F);
  static Color sugarWhite = const Color(0xFFF2FFED);
  static Color saladGreen = const Color(0xFF33BC0A);
  static Color snowWhite = const Color(0xFFFFFAEE);
  static Color eggYellow = const Color(0xFFFFB605);
}