class Strings {
  static String goalSectionTitle  = 'Start New Goal';
  static String seeAllAction  = 'See all';
  static String dailyTaskSectionTitle = 'Daily Task';
}